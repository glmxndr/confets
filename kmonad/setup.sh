#!/usr/bin/env bash

# aken from https://github.com/kmonad/kmonad/blob/master/doc/faq.md#linux
sudo groupadd uinput
sudo usermod -aG input $USER
sudo usermod -aG uinput $USER
sudo cp 90-uinput-kmonad.rules /etc/udev/rules.d/90-uinput-kmonad.rules

