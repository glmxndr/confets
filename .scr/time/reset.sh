#help: Resets both system clock and hardware clock from the internet.

# Reset system clock
sudo ntpd -qg
# Reset hardware clock from system clock
sudo hwclock --systohc

