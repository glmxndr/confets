echo -- Disabling aggregate thermal sensor which provokes shudowns
echo disabled | sudo tee /sys/class/thermal/thermal_zone0/mode

echo -- Enable natural scrolling
scr touchpad natural-scrolling

echo -- Auto-adjust time
scr time autoadjust

echo -- Silent beep
scr beep silent

echo -- Default kbd layout
scr layout default
