#synopsis: arg1 arg2
#help: Add helpful description here.
#help:   • arg1: first arg
#help:   • arg2: second arg

FILEPATH=$1
FILENAME=$(basename "$1")

sshpass -p "${REMARKABLE_PASSWORD}" \
  scp -q -o HostKeyAlgorithms=+ssh-rsa \
    "${FILEPATH}" \
    root@10.11.99.1:"/usr/share/remarkable/templates/${FILENAME}"

