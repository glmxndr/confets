#synopsis: line
#help: Remove the given line from ssh_known_hosts file.

LINE=$1

sed -i "${LINE}d" ~/.ssh/known_hosts

