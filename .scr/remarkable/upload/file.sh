#synopsis: file
#help: Upload a file to remarkable tablet.

FILE=$1
curl --form "file=@\"${FILE}\"" "http://10.11.99.1/upload"
