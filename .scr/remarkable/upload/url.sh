#synopsis: url name
#help: Upload a file at the given URL to remarkable tablet, with given name.

set -x

URL=$1
NAME=$2

FILE="/tmp/rm-upload/$NAME.pdf"

mkdir -p /tmp/rm-upload
curl "$URL" > "$FILE"

scr remarkable upload file "$FILE"
