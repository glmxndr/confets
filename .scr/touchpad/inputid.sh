#help: Find the id for touchpad device

xinput --list | grep TM3512-010 | sed -re 's/.*id=([^ \t]+).*/\1/'
