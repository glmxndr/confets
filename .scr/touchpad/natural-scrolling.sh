#help: Set natural scrolling for the touchpad 

xinput --set-prop "$(scr touchpad inputid)" "libinput Natural Scrolling Enabled" 1
